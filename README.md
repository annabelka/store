# CRUD for online store products #


## **Installation**


1. `git clone https://annabelka@bitbucket.org/annabelka/store.git`
2. `composer install`
3. Create configuration `.env` file
4. `php artisan key:generate`
5. `php artisan migrate:fresh --seed`
6. `npm install`