<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Variant;
use App\Models\Feature;
use App\Models\ProductFeature;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $features = Feature::all();
        $products = factory(Product::class, 50)->create();
        $products->each(function ($product) use ($features) {
            factory(Variant::class, 1)->create(['product_id' => $product->id]);
            $features->each(function ($feature) use ($product) {
                factory(ProductFeature::class, 1)->create([
                    'product_id' => $product->id,
                    'feature_id' => $feature->id
                ]);
            });
        });
       /* $order->each(function ($order) {
            $products = factory(Product::class, 5)->create(['order_id' => $order->id]);
            $products->map(function ($product) {
                $product->waste_types = WasteType::inRandomOrder()->pluck('id')->toArray();
                return $product;
            });
        });



        factory(App\Models\Product::class, 50)->create()->each(function ($product) {
            $product->posts()->save(factory(App\Post::class)->make());
        });*/
    }
}
