<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Variant;
use Faker\Generator as Faker;

$factory->define(Variant::class, function (Faker $faker) {
    return [
        'price' => $faker->randomFloat(2,10, 1000)
    ];
});
