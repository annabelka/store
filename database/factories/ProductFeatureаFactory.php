<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\ProductFeature;
use Faker\Generator as Faker;

$factory->define(ProductFeature::class, function (Faker $faker) {
    return [
        'value' => $faker->words(1, true)
    ];
});
