import Vue                  from 'vue';
import VueRouter            from 'vue-router';
import axios                from 'axios'
import VueAxios             from 'vue-axios'
import routs                from './modules/routs';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const router = new VueRouter({
    mode: 'history',
    routes: routs
});

router.beforeEach((to, from, next) => {
    next();
})

export default router