import Products from '../../components/Admin/Products'
import Product  from '../../components/Admin/Product'
export default [
    {
        path: '/admin/products',
        name: 'admin-products',
        component: Products,
    },
    {
        path: '/admin/product/:id?',
        name: 'product',
        component: Product,
    },
];