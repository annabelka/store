<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'variants' => 'required|array',
            'product_features' => 'required|array',
            'variants.*.price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'product_features.*.feature_id' => 'sometimes|nullable|integer',
            'product_features.*.value' => 'sometimes|nullable|string|max:255',
        ];
    }

    public function messages()
    {
        return [];
    }
}
