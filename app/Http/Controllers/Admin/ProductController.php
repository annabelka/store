<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\StoreRequest;
use App\Http\Requests\Admin\Product\UpdateRequest;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * ProductController constructor.
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $text = $request->filled('text') ? $request->input('text') : '';
        $products = $this->productService->index($text);
        return response()->json($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->only(['name', 'variants', 'product_features']);
        $product = $this->productService->store($data);
        $product = $this->productService->show($product);
        return response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return Response
     */
    public function show(Product $product)
    {
        $product = $this->productService->show($product);
        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Product $product
     * @return Response
     */
    public function update(UpdateRequest $request, Product $product)
    {
        $data = $request->only(['name', 'variants', 'product_features']);
        $this->productService->update($product, $data);
        $product = $this->productService->show($product);
        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product  $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        $this->productService->destroy($product);
        return response()->json();
    }
}
