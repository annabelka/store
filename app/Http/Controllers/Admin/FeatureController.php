<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\FeatureService;

class FeatureController extends Controller
{
    /**
     * @var FeatureService
     */
    private $featureService;

    /**
     * FeatureController constructor.
     * @param FeatureService $featureService
     */
    public function __construct(FeatureService $featureService)
    {
        $this->featureService = $featureService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $features = $this->featureService->index();
        return response()->json($features);
    }
}
