<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ProductFeature
 *
 * @property int $id
 * @property int $product_id
 * @property int $feature_id
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature whereFeatureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductFeature whereUpdatedAt($value)
 * @mixin \Illuminate\Database\Eloquent\
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Feature $feature
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product $product
 */
class ProductFeature extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products_features';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'feature_id',
        'value',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @return BelongsTo
     */
    public function feature(): BelongsTo
    {
        return $this->belongsTo(Feature::class);
    }

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
