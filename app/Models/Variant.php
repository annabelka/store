<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Variant
 *
 * @property int $id
 * @property int $product_id
 * @property string $price
 * @property string $currency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variant whereUpdatedAt($value)
 * @mixin \Illuminate\Database\Eloquent\
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product $product
 */
class Variant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'variants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'price',
        'currency'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
