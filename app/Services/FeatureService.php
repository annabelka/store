<?php

namespace App\Services;

use App\Models\Feature;
use Illuminate\Database\Eloquent\Collection;

class FeatureService
{
    /**
     * @return Collection
     */
    public function index(): Collection
    {
        return Feature::all();
    }
}