<?php

namespace App\Services;

use App\Models\Product;
use App\Models\ProductFeature;
use App\Models\Variant;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ProductService
{
    /**
     * @param string $text
     * @return LengthAwarePaginator
     */
    public function index(string $text = ''): LengthAwarePaginator
    {
        return Product::where('name', 'like', '%'.$text.'%')->with('variants')->latest()->paginate();
    }

    /**
     * @param array $data
     * @return Product
     */
    public function store(array $data): Product
    {
        $product = Product::create($this->getDataProduct($data));
        if (!empty($data['variants'])) {
            foreach ($data['variants'] as $variant) {
                $product->variants()->create($this->getDataVariant($variant));
            }
        }
        if (!empty($data['product_features'])) {
            foreach ($data['product_features'] as $productFeatures) {
                $product->productFeatures()->create($this->getDataProductFeature($productFeatures));
            }
        }
        return $product;
    }

    /**
     * @param Product $product
     * @return Product
     */
    public function show(Product $product): Product
    {
        $product->variants;
        foreach ($product->productFeatures as $key => $productFeature) {
            $product->productFeatures[$key]->name = $productFeature->feature->name;
        }
        return $product;
    }

    /**
     * @param Product $product
     * @param array $data
     */
    public function update(Product $product, array $data)
    {
        $product->update($this->getDataProduct($data));
        if (!empty($data['variants'])) {
            foreach ($data['variants'] as $variant) {
                Variant::whereId($variant['id'])->update($this->getDataVariant($variant));
            }
        }
        if (!empty($data['product_features'])) {
            foreach ($data['product_features'] as $productFeatures) {
                ProductFeature::whereId($productFeatures['id'])->update($this->getDataProductFeature($productFeatures));
            }
        }
    }

    public function destroy(Product $product)
    {
        $product->delete();
    }

    /**
     * @param array $data
     * @return array
     */
    public function getDataProduct(array $data): array
    {
        return [
          'name' => $data['name'],
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public function getDataVariant(array $data): array
    {
        return [
          'price' => $data['price'],
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public function getDataProductFeature(array $data): array
    {
        return [
          'feature_id' => $data['feature_id'],
          'value' => is_null($data['value']) ? '' : $data['value'],
        ];
    }
}