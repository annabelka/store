<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api', 'prefix' => 'admin', 'as' => 'admin.',], function ($router) {
    Route::prefix('products')->group(function () {
        Route::get('index/{text?}', 'Admin\ProductController@index')->name('products.index');
        Route::get('show/{product}', 'Admin\ProductController@show')->name('products.show');
        Route::put('update/{product}', 'Admin\ProductController@update')->name('products.update');
        Route::post('store', 'Admin\ProductController@store')->name('products.store');
        Route::delete('destroy/{product}', 'Admin\ProductController@destroy')->name('products.destroy');
    });
    Route::prefix('features')->group(function () {
        Route::get('index', 'Admin\FeatureController@index')->name('features.index');
    });

});
